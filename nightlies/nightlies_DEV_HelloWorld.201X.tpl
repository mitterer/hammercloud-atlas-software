Job (
 name = '' ,
 outputsandbox = [] ,
 info = JobInfo (
    ) ,
 inputdata = None ,
 merger = None ,
 inputsandbox = [ ] ,
 application = Athena (
    atlas_dbrelease = '' ,
    collect_stats = True ,
    atlas_release = '20.1.X' ,
    atlas_project = 'AtlasOffline_rel' ,
    atlas_production = '####NIGHTLYVER####' ,
    atlas_cmtconfig = 'x86_64-slc6-gcc48-opt' ,
    atlas_run_dir = './' ,
    atlas_exetype = 'ATHENA' ,
    athena_compile = False ,
    atlas_run_config = {'input': {'noInput': True}, 'other': {}, 'output': {'alloutputs': []}} ,
    option_file = [ File (
       name = '####JOBOPTIONS####' ,
       subdir = '.'
       ) , ] ,
    group_area = File (
    name = '' ,
    subdir = '.'
    ) ,
    user_area = File (
       name = '####USERAREA####' ,
       subdir = '.'
    ) 
    ) ,
 outputdata = DQ2OutputDataset (
    datasetname = '####OUTPUTDATASETNAME####',  
   ) ,
 splitter = GenericSplitter (
    attribute = 'comment' ,
    values = [ '42' ] 
   ) ,
 backend = Panda (
    nobuild = True ,
    site = ####SITES#### ,
    accessmode = '####INPUTTYPE####' 
    )  
 ) 
