Job (
 name = '' ,
 outputsandbox = [] ,
 info = JobInfo (
    ) ,
 inputdata = DQ2Dataset (
    match_ce_all = False ,
    datatype = 'MC' ,
    failover = False ,
    accessprotocol = '' ,
    guids = [] ,
    check_md5sum = False ,
    exclude_names = [] ,
    dataset = ####DATASET#### ,
    min_num_files = 0 ,
    tagdataset = [] ,
    names = [] ,
    number_of_files = 5 ,
    use_aodesd_backnav = False ,
    type = '####INPUTTYPE####'
    ) ,
 merger = None ,
 inputsandbox = [ ] ,
 application = Athena (
    atlas_dbrelease = '' ,
    collect_stats = True ,
    atlas_release = '20.1.X' ,
    atlas_project = 'AtlasOffline_rel' ,
    atlas_production = '####NIGHTLYVER####' ,
    atlas_cmtconfig = 'x86_64-slc6-gcc48-opt' ,
    max_events = 20 ,
    atlas_run_dir = './' ,
    atlas_exetype = 'TRF' ,
    athena_compile = False ,
    atlas_run_config = {'input': {}, 'other': {}, 'output': {}} ,
    option_file = [ File (
       name = '####JOBOPTIONS####' ,
       subdir = '.'
       ) , ] ,
    options = 'Reco_trf.py AMITag=q120 inputBSFile=%IN outputESDFile=%OUT.myESD.pool.root outputAODFile=%OUT.myAOD.pool.root outputTAGFile=%OUT.myTAG.root outputHISTFile=%OUT.myHIST.root maxEvents=10 preExec="rec.doFloatingPointException.set_Value_and_Lock(True)" autoConfiguration=everything' ,
    group_area = File (
    name = '' ,
    subdir = '.'
    ) ,
    user_area = File (
       name = '####USERAREA####' ,
       subdir = '.'
    ) 
    ) ,
 outputdata = DQ2OutputDataset (
    datasetname = '####OUTPUTDATASETNAME####',  
    outputdata = [ 'myESD.pool.root','myAOD.pool.root','myTAG.root', 'myHIST.root'] ,

   ) ,
 splitter = DQ2JobSplitter (
    update_siteindex = False ,
    numsubjobs = 1 ,
    use_lfc = False ,
    filesize = 0 ,
    use_blacklist = False ,
    numfiles = 1
    ) ,
 backend = Panda (
    nobuild = True ,
    site = ####SITES#### ,
    accessmode = '####INPUTTYPE####' 
    )  
 ) 
