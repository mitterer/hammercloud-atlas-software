configuretask.py
===


The `configuretask.py` script prepares draft of HammerCloud template
configuration file for JEDI task.

You need to provide JEDI taskID.

Howto
===
1. Pick a `JEDI task ID` of a task that you desire to run in
   HammerCloud.
2. Run the script: 

```
JEDITASKID=24235582
python configuretask.py --jeditaskid ${JEDITASKID}
```

Draft of the template file will be stored in a file. Filename and
output are configurable. Check `./configuretask.py --help`

Edit the template file if needed, i.e. check for any unique identifier
that need to be "template-ified" such as tasknames etc.  Then upload
the file to the desired location in hammercloud-atlas-inputfiles to
make it available for test submission.



validate_template.py
===


The `validate_template.py` script can be used to check for common
errors in template files. So far it only checks if the included json
objects can be properly decoded

Usage:
```
./validate_template.py [template_file_to_check]
```
